class Cola: 
    def __init__(self):
        self.items = []
    
    def encolar(self, x):
        self.items.append(x)

    def desencolar(self):
        try:
            return self.items.pop(0)
        except:
            raise ValueError("La cola esta vacia")
    
    def es_vacia(self):
        return self.items == []

q = Cola()
q.encolar(1)
q.encolar(2)
q.encolar(3)
print(q.es_vacia())
print(q.desencolar())
print(q.desencolar())
print(q.desencolar())
print(q.es_vacia())

