from pythonds.basic.queue import Queue

def hotPotato(nameList, N):
    queue = Queue()
    for name in nameList:
        queue.enqueue(name)
    
    while queue.size() > 1:
        for i in range(N):
            print(i)
            queue.enqueue(queue.dequeue())
        
        queue.dequeue()
    return queue.dequeue()

print(hotPotato(["Cesar", "Nayeli", "Sarahi", "Dante", "Emanuel", "Dylan"], 15))