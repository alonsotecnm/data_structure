class ColaDoble:
    def __init__(self):
        self.items = []

    def isEmpty(self):
        return self.items == []

    def addToFront(self, item):
        self.items.append(item)

    def addToFinal(self, item):
        self.items.insert(0,item)

    def removeFront(self):
        return self.items.pop()

    def removeFinal(self):
        return self.items.pop(0)

    def size(self):
        return len(self.items)

d = ColaDoble()
print(d.isEmpty())
d.addToFinal(4)
d.addToFinal('perro')
d.addToFinal('gato')
d.addToFront(True)
print(d.size())