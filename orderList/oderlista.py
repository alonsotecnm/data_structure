#1200, 750, 820, 550, 490

sueldos=[]
for x in range(5):
    valor=int(input("Ingresa tu sueldo:"))
    sueldos.append(valor)

print("Lista sin ordenar")
print(sueldos)

for x in range(4):
    if sueldos[x]>sueldos[x+1]:
        aux=sueldos[x]
        sueldos[x]=sueldos[x+1]
        sueldos[x+1]=aux
        
print("Lista con el ultimo valor ordenado")
print(sueldos)

#Cuando !x    x=0   x=1  x=2
#       1200 750   750   750
#       750  1200  820   820
#       820  820   1200  550
#       550  550   550   1200
#       490  490   490   490