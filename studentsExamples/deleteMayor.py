print("Este programa genera dos listas, en la primera guarda los nombres de los empleados de una empresa, en la segunda guarda sus respectivos sueldos. El programa borra los datos de los empleados con un sueldo mayor a 10000")

empleados = []
sueldos = []
compare = 10000.0

veces = int(input("Cuantos empleados va a ingresar? "))

for x in range(veces):
    empleado = input("Introduzca el nombre del empleado: ")
    empleados.append(empleado)
    sueldo = float(input("Ingrese el sueldo de " + empleado + " "))
    sueldos.append(sueldo)

print("Estos son los datos de las listas: ")
print(empleados)
print(sueldos)

for x in range(veces-1):
    print(veces)
    if sueldos[x] > compare:
        sueldos.pop(x)
        empleados.pop(x)

print("\nEstas son las listas despues de eliminar los datos de los empleados que ganan mas de 10000 pesos")
print(empleados)
print(sueldos)