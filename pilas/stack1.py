from pythonds.basic.stack import Stack


def revstring(mystr):

    myStack = Stack() # this is how i have myStack

    for ch in mystr: # looping through characters in my string
        myStack.push(ch) # push the characters to form a stack

    revstr = '' # form an empty reverse string
    while not myStack.isEmpty():
        # adding my characters to the empty reverse string in reverse order
        revstr = revstr + myStack.pop()

    return revstr

print(revstring("alonso"))