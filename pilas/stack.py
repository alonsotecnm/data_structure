class Stack:
    def __init__(self):
        self.items = []

    def push(self, item):
        self.items.append(item)

    def pop(self):
        return self.items.pop()

    def peek(self):
        return self.items[-1]

    def isEmpty(self):
        return len(self.items) == 0

def reverse(text):
    s = Stack()
    for c in text:
        s.push(c)
        revText = ""
    while not s.isEmpty():
        revText += s.pop()
        return revText

#test code
print(reverse("abcde"))