class Pila: 
    def __init__(self):
        self.items = []
    
    def isEmpty(self):
        return self.items == []
    
    def addItem(self, item):
        self.items.insert(0, item)
    
    def popItem(self):
        return self.items.pop(0)
    
    def inspect(self):
        return self.items[0]
    
    def size(self):
        return len(self.items)

def reverse(text):
    s = Pila()
    for c in text:
        s.addItem(c)
        revText = ""
    while not s.isEmpty():
        revText += s.popItem()
        return revText